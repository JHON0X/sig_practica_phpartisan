<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class cost extends Model
{
    protected $table= 'cost';
    protected $primarykey= 'id_cost';
    public $timestamps= true;
    const CREATED_AT = 'date_create';
    const UPDATED_AT = 'date_upadate';

    protected $fillable= [
        'price',
        'descrip',

    ];
}
